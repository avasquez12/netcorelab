﻿using AppNetCoreLab.Models;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppNetCoreLab.Config
{
    public static class ServiceConfig
    {
        public static IServiceCollection AddRegistration(this IServiceCollection services)
        {
            AddRegisterServices(services);
            AddRegisterRepositories(services);
            return services;
        }

        private static IServiceCollection AddRegisterServices(IServiceCollection services)
        {
            //services.AddTransient<IAuthenticationClientService, AuthenticationClientService>();

            return services;
        }

        private static IServiceCollection AddRegisterRepositories(IServiceCollection services)
        {
            // Security
            //services.AddTransient<IAuthenticationRepository, AuthenticationRepository>();

            // Save
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppNetCoreLab.Models
{
    public class Person
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime DateOfBirth { get; set; }
        public long CityId { get; set; }


        // Virtual objects
        public City City { get; set; }
    }
}

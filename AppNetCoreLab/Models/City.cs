﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppNetCoreLab.Models
{
    public class City
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long PersonId { get; set; }


        // Virtual Objects
        public IList<Person> People { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppNetCoreLab.Models
{
    public interface IUnitOfWork
    {
        Task SaveChanges();
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly LabContext _context;

        public UnitOfWork(LabContext context)
        {
            _context = context;
        }

        public Task SaveChanges()
        {
            return _context.SaveChangesAsync();
        }
    }
}

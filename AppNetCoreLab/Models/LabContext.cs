﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppNetCoreLab.Models
{
    public class LabContext : DbContext
    {
        // DBSETS
        public DbSet<Person> Person { get; set; }
        public DbSet<City> City { get; set; }


        public LabContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Deal
            //CategoryConfig.SetEntityBuilder(modelBuilder.Entity<Category>());

            base.OnModelCreating(modelBuilder);
        }
    }
}
